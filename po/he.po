# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# Omer I.S. <omeritzicschwartz@gmail.com>, 2021.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-04-04 20:42+0200\n"
"PO-Revision-Date: 2021-02-17 17:57+0200\n"
"Last-Translator: Omer I.S. <omeritzicschwartz@gmail.com>\n"
"Language-Team: \n"
"Language: he\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n==2 ? 1 : n>10 && n%10==0 ? "
"2 : 3);\n"
"X-Generator: Poedit 2.4.2\n"

#: askRenamePopup.js:42
msgid "Folder name"
msgstr "שם התיקייה"

#: askRenamePopup.js:42
msgid "File name"
msgstr "שם הקובץ"

#: askRenamePopup.js:49 desktopManager.js:791
msgid "OK"
msgstr "אישור"

#: askRenamePopup.js:49
msgid "Rename"
msgstr "שינוי שם"

#: dbusUtils.js:66
msgid "\"${programName}\" is needed for Desktop Icons"
msgstr ""

#: dbusUtils.js:67
msgid ""
"For this functionality to work in Desktop Icons, you must install "
"\"${programName}\" in your system."
msgstr ""

#: desktopIconsUtil.js:96
msgid "Command not found"
msgstr "הפקודה לא נמצאה"

#: desktopManager.js:214
msgid "Nautilus File Manager not found"
msgstr "מנהל הקבצים Nautilus לא נמצא"

#: desktopManager.js:215
msgid "The Nautilus File Manager is mandatory to work with Desktop Icons NG."
msgstr ""

#: desktopManager.js:754
msgid "Clear Current Selection before New Search"
msgstr ""

#: desktopManager.js:793 fileItemMenu.js:369
msgid "Cancel"
msgstr "ביטול"

#: desktopManager.js:795
msgid "Find Files on Desktop"
msgstr ""

#: desktopManager.js:860 desktopManager.js:1483
msgid "New Folder"
msgstr "תיקיה חדשה"

#: desktopManager.js:864
msgid "New Document"
msgstr "מסמך חדש"

#: desktopManager.js:869
msgid "Paste"
msgstr "הדבקה"

#: desktopManager.js:873
msgid "Undo"
msgstr "ביטול פעולה"

#: desktopManager.js:877
msgid "Redo"
msgstr "ביצוע חוזר"

#: desktopManager.js:883
#, fuzzy
msgid "Select All"
msgstr "לבחור הכול"

#: desktopManager.js:891
msgid "Show Desktop in Files"
msgstr ""

#: desktopManager.js:895 fileItemMenu.js:287
msgid "Open in Terminal"
msgstr "פתיחה במסוף"

#: desktopManager.js:901
msgid "Change Background…"
msgstr "שינוי הרקע…"

#: desktopManager.js:912
#, fuzzy
msgid "Desktop Icons Settings"
msgstr "הגדרות תצוגה"

#: desktopManager.js:916
msgid "Display Settings"
msgstr "הגדרות תצוגה"

#: desktopManager.js:1541
msgid "Arrange Icons"
msgstr ""

#: desktopManager.js:1545
msgid "Arrange By..."
msgstr ""

#: desktopManager.js:1554
msgid "Keep Arranged..."
msgstr ""

#: desktopManager.js:1558
msgid "Keep Stacked by type..."
msgstr ""

#: desktopManager.js:1563
msgid "Sort Home/Drives/Trash..."
msgstr ""

#: desktopManager.js:1569
msgid "Sort by Name"
msgstr ""

#: desktopManager.js:1571
msgid "Sort by Name Descending"
msgstr ""

#: desktopManager.js:1574
msgid "Sort by Modified Time"
msgstr ""

#: desktopManager.js:1577
msgid "Sort by Type"
msgstr ""

#: desktopManager.js:1580
msgid "Sort by Size"
msgstr ""

#. TRANSLATORS: "Home" is the text that will be shown in the user's personal folder
#: fileItem.js:156
msgid "Home"
msgstr "בית"

#: fileItem.js:275
msgid "Broken Link"
msgstr ""

#: fileItem.js:276
msgid "Can not open this File because it is a Broken Symlink"
msgstr ""

#: fileItem.js:326
msgid "Broken Desktop File"
msgstr ""

#: fileItem.js:327
msgid ""
"This .desktop file has errors or points to a program without permissions. It "
"can not be executed.\n"
"\n"
"\t<b>Edit the file to set the correct executable Program.</b>"
msgstr ""

#: fileItem.js:333
msgid "Invalid Permissions on Desktop File"
msgstr ""

#: fileItem.js:334
msgid ""
"This .desktop File has incorrect Permissions. Right Click to edit "
"Properties, then:\n"
msgstr ""

#: fileItem.js:336
msgid ""
"\n"
"<b>Set Permissions, in \"Others Access\", \"Read Only\" or \"None\"</b>"
msgstr ""

#: fileItem.js:339
msgid ""
"\n"
"<b>Enable option, \"Allow Executing File as a Program\"</b>"
msgstr ""

#: fileItem.js:347
msgid ""
"This .desktop file is not trusted, it can not be launched. To enable "
"launching, right-click, then:\n"
"\n"
"<b>Enable \"Allow Launching\"</b>"
msgstr ""

#: fileItemMenu.js:118
msgid "Open All..."
msgstr "לפתוח הכול..."

#: fileItemMenu.js:118
msgid "Open"
msgstr "פתיחה"

#: fileItemMenu.js:129
msgid "Stack This Type"
msgstr ""

#: fileItemMenu.js:129
msgid "Unstack This Type"
msgstr ""

#: fileItemMenu.js:141
msgid "Scripts"
msgstr "תסריטים"

#: fileItemMenu.js:147
msgid "Open All With Other Application..."
msgstr "לפתוח הכול באמצעות יישום אחר..."

#: fileItemMenu.js:147
msgid "Open With Other Application"
msgstr "פתיחה באמצעות יישום אחר"

#: fileItemMenu.js:153
msgid "Launch using Dedicated Graphics Card"
msgstr ""

#: fileItemMenu.js:162
msgid "Run as a program"
msgstr ""

#: fileItemMenu.js:170
msgid "Cut"
msgstr "גזירה"

#: fileItemMenu.js:175
msgid "Copy"
msgstr "העתקה"

#: fileItemMenu.js:181
msgid "Rename…"
msgstr "שינוי שם…"

#: fileItemMenu.js:189
msgid "Move to Trash"
msgstr "העברה לאשפה"

#: fileItemMenu.js:195
msgid "Delete permanently"
msgstr "מחיקה לצמיתות"

#: fileItemMenu.js:203
msgid "Don't Allow Launching"
msgstr "לא לאפשר הפעלה"

#: fileItemMenu.js:203
msgid "Allow Launching"
msgstr "לאפשר הפעלה"

#: fileItemMenu.js:214
msgid "Empty Trash"
msgstr "פינוי האשפה"

#: fileItemMenu.js:225
msgid "Eject"
msgstr "שליפה"

#: fileItemMenu.js:231
msgid "Unmount"
msgstr "ניתוק"

#: fileItemMenu.js:241
msgid "Extract Here"
msgstr ""

#: fileItemMenu.js:245
msgid "Extract To..."
msgstr ""

#: fileItemMenu.js:252
msgid "Send to..."
msgstr "שליחה אל..."

#: fileItemMenu.js:258
msgid "Compress {0} file"
msgid_plural "Compress {0} files"
msgstr[0] "דחיסת קובץ {0}"
msgstr[1] "דחיסת {0} קבצים"
msgstr[2] "דחיסת {0} קבצים"
msgstr[3] "דחיסת {0} קבצים"

#: fileItemMenu.js:264
msgid "New Folder with {0} item"
msgid_plural "New Folder with {0} items"
msgstr[0] "תיקיה חדשה עם פריט {0}"
msgstr[1] "תיקיה חדשה עם {0} פריטים"
msgstr[2] "תיקיה חדשה עם {0} פריטים"
msgstr[3] "תיקיה חדשה עם {0} פריטים"

#: fileItemMenu.js:273
msgid "Common Properties"
msgstr ""

#: fileItemMenu.js:273
msgid "Properties"
msgstr "מאפיינים"

#: fileItemMenu.js:280
msgid "Show All in Files"
msgstr ""

#: fileItemMenu.js:280
msgid "Show in Files"
msgstr ""

#: fileItemMenu.js:365
msgid "Select Extract Destination"
msgstr ""

#: fileItemMenu.js:370
#, fuzzy
msgid "Select"
msgstr "לבחור הכול"

#: fileItemMenu.js:396
msgid "Can not email a Directory"
msgstr ""

#: fileItemMenu.js:397
msgid "Selection includes a Directory, compress the directory to a file first."
msgstr ""

#: preferences.js:67
msgid "Settings"
msgstr "הגדרות"

#: prefswindow.js:46
msgid "Size for the desktop icons"
msgstr "גודל הסמלים בשולחן העבודה"

#: prefswindow.js:46
msgid "Tiny"
msgstr "פצפון"

#: prefswindow.js:46
msgid "Small"
msgstr "קטן"

#: prefswindow.js:46
msgid "Standard"
msgstr "רגיל"

#: prefswindow.js:46
msgid "Large"
msgstr "גדול"

#: prefswindow.js:47
#, fuzzy
msgid "Show the personal folder in the desktop"
msgstr "הצגת התיקייה האישית"

#: prefswindow.js:48
#, fuzzy
msgid "Show the trash icon in the desktop"
msgstr "הצגת סמל האשפה"

#: prefswindow.js:49 schemas/org.gnome.shell.extensions.ding.gschema.xml:45
msgid "Show external drives in the desktop"
msgstr ""

#: prefswindow.js:50 schemas/org.gnome.shell.extensions.ding.gschema.xml:50
msgid "Show network drives in the desktop"
msgstr ""

#: prefswindow.js:53
msgid "New icons alignment"
msgstr ""

#: prefswindow.js:54
msgid "Top-left corner"
msgstr ""

#: prefswindow.js:55
msgid "Top-right corner"
msgstr ""

#: prefswindow.js:56
msgid "Bottom-left corner"
msgstr ""

#: prefswindow.js:57
msgid "Bottom-right corner"
msgstr ""

#: prefswindow.js:59 schemas/org.gnome.shell.extensions.ding.gschema.xml:55
msgid "Add new drives to the opposite side of the screen"
msgstr ""

#: prefswindow.js:60
msgid "Highlight the drop place during Drag'n'Drop"
msgstr ""

#: prefswindow.js:61 schemas/org.gnome.shell.extensions.ding.gschema.xml:90
msgid "Use Nemo to open folders"
msgstr ""

#: prefswindow.js:63
msgid "Add an emblem to soft links"
msgstr ""

#: prefswindow.js:65
msgid "Use dark text in icon labels"
msgstr ""

#. Nautilus options
#: prefswindow.js:71
msgid "Settings shared with Nautilus"
msgstr ""

#: prefswindow.js:90
msgid "Click type for open files"
msgstr ""

#: prefswindow.js:90
msgid "Single click"
msgstr "לחיצה יחידה"

#: prefswindow.js:90
msgid "Double click"
msgstr "לחיצה כפולה"

#: prefswindow.js:91
msgid "Show hidden files"
msgstr "הצגת קבצים מוסתרים"

#: prefswindow.js:92
msgid "Show a context menu item to delete permanently"
msgstr ""

#: prefswindow.js:97
msgid "Action to do when launching a program from the desktop"
msgstr ""

#: prefswindow.js:98
msgid "Display the content of the file"
msgstr ""

#: prefswindow.js:99
msgid "Launch the file"
msgstr "הפעלת הקובץ"

#: prefswindow.js:100
msgid "Ask what to do"
msgstr "לשאול מה לעשות"

#: prefswindow.js:106
msgid "Show image thumbnails"
msgstr "הצגת תמונות מממוזערות"

#: prefswindow.js:107
msgid "Never"
msgstr "אף פעם"

#: prefswindow.js:108
msgid "Local files only"
msgstr "לקבצים מקומיים בלבד"

#: prefswindow.js:109
msgid "Always"
msgstr "תמיד"

#: showErrorPopup.js:40
msgid "Close"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:25
msgid "Icon size"
msgstr "גודל הסמלים"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:26
msgid "Set the size for the desktop icons."
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:30
msgid "Show personal folder"
msgstr "הצגת התיקייה האישית"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:31
msgid "Show the personal folder in the desktop."
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:35
msgid "Show trash icon"
msgstr "הצגת סמל האשפה"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:36
msgid "Show the trash icon in the desktop."
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:40
msgid "New icons start corner"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:41
msgid "Set the corner from where the icons will start to be placed."
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:46
msgid "Show the disk drives connected to the computer."
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:51
msgid "Show mounted network volumes in the desktop."
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:56
msgid ""
"When adding drives and volumes to the desktop, add them to the opposite side "
"of the screen."
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:60
msgid "Shows a rectangle in the destination place during DnD"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:61
msgid ""
"When doing a Drag'n'Drop operation, marks the place in the grid where the "
"icon will be put with a semitransparent rectangle."
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:65
msgid "Sort Special Folders - Home/Trash Drives."
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:66
msgid ""
"When arranging Icons on desktop, to sort and change the position of the "
"Home, Trash and mounted Network or External Drives"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:70
msgid "Keep Icons Arranged"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:71
msgid "Always keep Icons Arranged by the last arranged order"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:75
msgid "Arrange Order"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:76
msgid "Icons Arranged by this property"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:80
msgid "Keep Icons Stacked"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:81
msgid "Always keep Icons Stacked, Similar types are grouped"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:85
msgid "Type of Files to not Stack"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:86
msgid "An Array of strings types, Don't Stack these types of files"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:91
msgid "Use Nemo instead of Nautilus to open folders."
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:95
msgid "Add an emblem to links"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:96
msgid "Add an emblem to allow to identify soft links."
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:100
msgid "Use black for label text"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:101
msgid ""
"Paint the label text in black instead of white. Useful when using light "
"backgrounds."
msgstr ""

#~ msgid "Do you want to run “{0}”, or display its contents?"
#~ msgstr "האם ברצונך להריץ את \"{0}\", או להציג את תוכנו?"

#~ msgid "“{0}” is an executable text file."
#~ msgstr "„{0}” הוא קובץ טקסט הניתן להפעלה."

#~ msgid "Execute in a terminal"
#~ msgstr "הרצה במסוף"

#~ msgid "Show"
#~ msgstr "הצגה"

#~ msgid "Execute"
#~ msgstr "הרצה"

#~ msgid "Delete"
#~ msgstr "מחיקה"

#~ msgid "Error while deleting files"
#~ msgstr "שגיאה במחיקת הקבצים"

#~ msgid "Are you sure you want to permanently delete these items?"
#~ msgstr "האם אכן ברצונך למחוק פריטים אלו לצמיתות?"

#~ msgid "If you delete an item, it will be permanently lost."
#~ msgstr "פריט שימחק יאבד לנצח."

#~ msgid "New folder"
#~ msgstr "תיקיה חדשה"
