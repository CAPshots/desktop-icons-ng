# Chinese (Taiwan) translation for desktop-icons.
# Copyright (C) 2018 desktop-icons's COPYRIGHT HOLDER
# This file is distributed under the same license as the desktop-icons package.
# Yi-Jyun Pan <pan93412@gmail.com>, 2018.
#
msgid ""
msgstr ""
"Project-Id-Version: desktop-icons master\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-04-04 20:42+0200\n"
"PO-Revision-Date: 2021-07-11 21:08+0800\n"
"Last-Translator: pan93412 <pan93412@gmail.com>\n"
"Language-Team: Chinese (Taiwan) <chinese-l10n@googlegroups.com>\n"
"Language: zh_TW\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.4.3\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: askRenamePopup.js:42
msgid "Folder name"
msgstr "資料夾名稱"

#: askRenamePopup.js:42
msgid "File name"
msgstr "檔案名稱"

#: askRenamePopup.js:49 desktopManager.js:791
msgid "OK"
msgstr "確定"

#: askRenamePopup.js:49
msgid "Rename"
msgstr "重新命名"

#: dbusUtils.js:66
msgid "\"${programName}\" is needed for Desktop Icons"
msgstr ""

#: dbusUtils.js:67
msgid ""
"For this functionality to work in Desktop Icons, you must install "
"\"${programName}\" in your system."
msgstr ""

#: desktopIconsUtil.js:96
msgid "Command not found"
msgstr "無法找到指令"

#: desktopManager.js:214
msgid "Nautilus File Manager not found"
msgstr "無法找到《檔案 (Nautilus 瀏覽器)》"

#: desktopManager.js:215
msgid "The Nautilus File Manager is mandatory to work with Desktop Icons NG."
msgstr "《檔案》強制與Desktop Icons NG 運行"

#: desktopManager.js:754
msgid "Clear Current Selection before New Search"
msgstr ""

#: desktopManager.js:793 fileItemMenu.js:369
msgid "Cancel"
msgstr "取消"

#: desktopManager.js:795
msgid "Find Files on Desktop"
msgstr ""

#: desktopManager.js:860 desktopManager.js:1483
msgid "New Folder"
msgstr "新增資料夾"

#: desktopManager.js:864
msgid "New Document"
msgstr "新增文件"

#: desktopManager.js:869
msgid "Paste"
msgstr "貼上"

#: desktopManager.js:873
msgid "Undo"
msgstr "復原"

#: desktopManager.js:877
msgid "Redo"
msgstr "重做"

#: desktopManager.js:883
#, fuzzy
msgid "Select All"
msgstr "全部選取"

#: desktopManager.js:891
#, fuzzy
msgid "Show Desktop in Files"
msgstr "在《檔案》中開啟桌面"

#: desktopManager.js:895 fileItemMenu.js:287
msgid "Open in Terminal"
msgstr "開啟終端器"

#: desktopManager.js:901
msgid "Change Background…"
msgstr "變更背景圖片…"

#: desktopManager.js:912
#, fuzzy
msgid "Desktop Icons Settings"
msgstr "桌面圖示設定"

#: desktopManager.js:916
msgid "Display Settings"
msgstr "顯示設定"

#: desktopManager.js:1541
msgid "Arrange Icons"
msgstr ""

#: desktopManager.js:1545
msgid "Arrange By..."
msgstr ""

#: desktopManager.js:1554
msgid "Keep Arranged..."
msgstr ""

#: desktopManager.js:1558
msgid "Keep Stacked by type..."
msgstr ""

#: desktopManager.js:1563
msgid "Sort Home/Drives/Trash..."
msgstr ""

#: desktopManager.js:1569
msgid "Sort by Name"
msgstr ""

#: desktopManager.js:1571
msgid "Sort by Name Descending"
msgstr ""

#: desktopManager.js:1574
msgid "Sort by Modified Time"
msgstr ""

#: desktopManager.js:1577
msgid "Sort by Type"
msgstr ""

#: desktopManager.js:1580
msgid "Sort by Size"
msgstr ""

#. TRANSLATORS: "Home" is the text that will be shown in the user's personal folder
#: fileItem.js:156
msgid "Home"
msgstr "家目錄"

#: fileItem.js:275
msgid "Broken Link"
msgstr ""

#: fileItem.js:276
msgid "Can not open this File because it is a Broken Symlink"
msgstr ""

#: fileItem.js:326
#, fuzzy
msgid "Broken Desktop File"
msgstr "在《檔案》中開啟桌面"

#: fileItem.js:327
msgid ""
"This .desktop file has errors or points to a program without permissions. It "
"can not be executed.\n"
"\n"
"\t<b>Edit the file to set the correct executable Program.</b>"
msgstr ""

#: fileItem.js:333
msgid "Invalid Permissions on Desktop File"
msgstr ""

#: fileItem.js:334
msgid ""
"This .desktop File has incorrect Permissions. Right Click to edit "
"Properties, then:\n"
msgstr ""

#: fileItem.js:336
msgid ""
"\n"
"<b>Set Permissions, in \"Others Access\", \"Read Only\" or \"None\"</b>"
msgstr ""

#: fileItem.js:339
msgid ""
"\n"
"<b>Enable option, \"Allow Executing File as a Program\"</b>"
msgstr ""

#: fileItem.js:347
msgid ""
"This .desktop file is not trusted, it can not be launched. To enable "
"launching, right-click, then:\n"
"\n"
"<b>Enable \"Allow Launching\"</b>"
msgstr ""

#: fileItemMenu.js:118
msgid "Open All..."
msgstr "開啟所有項目……"

#: fileItemMenu.js:118
msgid "Open"
msgstr "開啟"

#: fileItemMenu.js:129
msgid "Stack This Type"
msgstr ""

#: fileItemMenu.js:129
msgid "Unstack This Type"
msgstr ""

#: fileItemMenu.js:141
msgid "Scripts"
msgstr "命令稿"

#: fileItemMenu.js:147
msgid "Open All With Other Application..."
msgstr "用其他應用程式開啟所有……"

#: fileItemMenu.js:147
msgid "Open With Other Application"
msgstr "用其他應用程式開啟"

#: fileItemMenu.js:153
msgid "Launch using Dedicated Graphics Card"
msgstr ""

#: fileItemMenu.js:162
msgid "Run as a program"
msgstr ""

#: fileItemMenu.js:170
msgid "Cut"
msgstr "剪下"

#: fileItemMenu.js:175
msgid "Copy"
msgstr "複製"

#: fileItemMenu.js:181
msgid "Rename…"
msgstr "重新命名…"

#: fileItemMenu.js:189
msgid "Move to Trash"
msgstr "移動到垃圾桶"

#: fileItemMenu.js:195
msgid "Delete permanently"
msgstr "永久刪除"

#: fileItemMenu.js:203
msgid "Don't Allow Launching"
msgstr "不允許執行"

#: fileItemMenu.js:203
msgid "Allow Launching"
msgstr "允許執行"

#: fileItemMenu.js:214
msgid "Empty Trash"
msgstr "清空垃圾桶"

#: fileItemMenu.js:225
msgid "Eject"
msgstr "退出"

#: fileItemMenu.js:231
msgid "Unmount"
msgstr "卸載"

#: fileItemMenu.js:241
msgid "Extract Here"
msgstr "在這裡解開"

#: fileItemMenu.js:245
msgid "Extract To..."
msgstr "解開到…"

#: fileItemMenu.js:252
msgid "Send to..."
msgstr "傳送到…"

#: fileItemMenu.js:258
msgid "Compress {0} file"
msgid_plural "Compress {0} files"
msgstr[0] "壓縮 {0} 檔案"

#: fileItemMenu.js:264
#, fuzzy
msgid "New Folder with {0} item"
msgid_plural "New Folder with {0} items"
msgstr[0] "新增空白的（有 {0} 項目）資料夾"

#: fileItemMenu.js:273
msgid "Common Properties"
msgstr "共同屬性"

#: fileItemMenu.js:273
msgid "Properties"
msgstr "屬性"

#: fileItemMenu.js:280
msgid "Show All in Files"
msgstr "在《檔案》中顯示"

#: fileItemMenu.js:280
msgid "Show in Files"
msgstr "在《檔案》中顯示"

#: fileItemMenu.js:365
msgid "Select Extract Destination"
msgstr "選擇解壓縮的位置"

#: fileItemMenu.js:370
msgid "Select"
msgstr "選擇"

#: fileItemMenu.js:396
msgid "Can not email a Directory"
msgstr "無法寄送資料夾"

#: fileItemMenu.js:397
msgid "Selection includes a Directory, compress the directory to a file first."
msgstr "已選項目含資料夾，先把資料夾壓縮成一個檔案。"

#: preferences.js:67
msgid "Settings"
msgstr "設定"

#: prefswindow.js:46
msgid "Size for the desktop icons"
msgstr "桌面圖示的大小"

#: prefswindow.js:46
msgid "Tiny"
msgstr "更小圖示"

#: prefswindow.js:46
msgid "Small"
msgstr "小圖示"

#: prefswindow.js:46
msgid "Standard"
msgstr "適中圖示"

#: prefswindow.js:46
msgid "Large"
msgstr "大圖示"

#: prefswindow.js:47
msgid "Show the personal folder in the desktop"
msgstr "在桌面顯示個人資料夾"

#: prefswindow.js:48
msgid "Show the trash icon in the desktop"
msgstr "在桌面顯示垃圾桶圖示"

#: prefswindow.js:49 schemas/org.gnome.shell.extensions.ding.gschema.xml:45
msgid "Show external drives in the desktop"
msgstr "在桌面顯示外接硬碟"

#: prefswindow.js:50 schemas/org.gnome.shell.extensions.ding.gschema.xml:50
msgid "Show network drives in the desktop"
msgstr "在桌面顯示網路資料夾"

#: prefswindow.js:53
msgid "New icons alignment"
msgstr "新圖示排序位置"

#: prefswindow.js:54
msgid "Top-left corner"
msgstr "左上角"

#: prefswindow.js:55
msgid "Top-right corner"
msgstr "右上角"

#: prefswindow.js:56
msgid "Bottom-left corner"
msgstr "左下角"

#: prefswindow.js:57
msgid "Bottom-right corner"
msgstr "右下角"

#: prefswindow.js:59 schemas/org.gnome.shell.extensions.ding.gschema.xml:55
msgid "Add new drives to the opposite side of the screen"
msgstr "將新掛載硬碟置於螢幕的另一邊"

#: prefswindow.js:60
msgid "Highlight the drop place during Drag'n'Drop"
msgstr "拖曳時強調顯示拖曳後位置"

#: prefswindow.js:61 schemas/org.gnome.shell.extensions.ding.gschema.xml:90
msgid "Use Nemo to open folders"
msgstr ""

#: prefswindow.js:63
msgid "Add an emblem to soft links"
msgstr ""

#: prefswindow.js:65
msgid "Use dark text in icon labels"
msgstr ""

#. Nautilus options
#: prefswindow.js:71
msgid "Settings shared with Nautilus"
msgstr "與《檔案》共用設定"

#: prefswindow.js:90
msgid "Click type for open files"
msgstr "開啟檔案時的點擊方式"

#: prefswindow.js:90
msgid "Single click"
msgstr "單擊"

#: prefswindow.js:90
msgid "Double click"
msgstr "雙擊"

#: prefswindow.js:91
msgid "Show hidden files"
msgstr "顯示隱藏檔"

#: prefswindow.js:92
msgid "Show a context menu item to delete permanently"
msgstr "在快速選單顯示永久刪除"

#: prefswindow.js:97
msgid "Action to do when launching a program from the desktop"
msgstr "在桌面執行應用程式時的動作"

#: prefswindow.js:98
msgid "Display the content of the file"
msgstr "顯示檔案內容"

#: prefswindow.js:99
msgid "Launch the file"
msgstr "執行檔案"

#: prefswindow.js:100
msgid "Ask what to do"
msgstr "詢問要做什麼"

#: prefswindow.js:106
msgid "Show image thumbnails"
msgstr "顯示影像縮圖"

#: prefswindow.js:107
msgid "Never"
msgstr "從不"

#: prefswindow.js:108
msgid "Local files only"
msgstr "顯示本機檔案"

#: prefswindow.js:109
msgid "Always"
msgstr "總是"

#: showErrorPopup.js:40
msgid "Close"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:25
msgid "Icon size"
msgstr "圖示大小"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:26
msgid "Set the size for the desktop icons."
msgstr "設定桌面圖示的大小。"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:30
msgid "Show personal folder"
msgstr "顯示個人資料夾"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:31
msgid "Show the personal folder in the desktop."
msgstr "在桌面顯示個人資料夾。"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:35
msgid "Show trash icon"
msgstr "顯示垃圾桶圖示"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:36
msgid "Show the trash icon in the desktop."
msgstr "在桌面顯示垃圾桶圖示。"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:40
msgid "New icons start corner"
msgstr "新圖示排序方式"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:41
msgid "Set the corner from where the icons will start to be placed."
msgstr "設定新圖示置放的位置"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:46
msgid "Show the disk drives connected to the computer."
msgstr "顯示連接於本機的硬碟"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:51
#, fuzzy
msgid "Show mounted network volumes in the desktop."
msgstr "在桌面顯示個人資料夾。"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:56
msgid ""
"When adding drives and volumes to the desktop, add them to the opposite side "
"of the screen."
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:60
msgid "Shows a rectangle in the destination place during DnD"
msgstr "拖曳同時在拖曳目的位置顯示矩形方格"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:61
msgid ""
"When doing a Drag'n'Drop operation, marks the place in the grid where the "
"icon will be put with a semitransparent rectangle."
msgstr "當執行拖曳，在圖示即將被置放的位置用半透明矩形顯示方格"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:65
msgid "Sort Special Folders - Home/Trash Drives."
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:66
msgid ""
"When arranging Icons on desktop, to sort and change the position of the "
"Home, Trash and mounted Network or External Drives"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:70
msgid "Keep Icons Arranged"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:71
msgid "Always keep Icons Arranged by the last arranged order"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:75
msgid "Arrange Order"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:76
msgid "Icons Arranged by this property"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:80
msgid "Keep Icons Stacked"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:81
msgid "Always keep Icons Stacked, Similar types are grouped"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:85
msgid "Type of Files to not Stack"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:86
msgid "An Array of strings types, Don't Stack these types of files"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:91
msgid "Use Nemo instead of Nautilus to open folders."
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:95
msgid "Add an emblem to links"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:96
msgid "Add an emblem to allow to identify soft links."
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:100
msgid "Use black for label text"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:101
msgid ""
"Paint the label text in black instead of white. Useful when using light "
"backgrounds."
msgstr ""

#~ msgid ""
#~ "To configure Desktop Icons NG, do right-click in the desktop and choose "
#~ "the last item: 'Desktop Icons settings'"
#~ msgstr ""
#~ "要設定Desktop Icons NG，在桌面按滑鼠右鍵並選擇最後項目「桌面圖示設定」"

#~ msgid "Do you want to run “{0}”, or display its contents?"
#~ msgstr "要執行「{0}」，還是顯示它的內容？"

#~ msgid "“{0}” is an executable text file."
#~ msgstr "「{0}」是可執行的文字檔。"

#~ msgid "Execute in a terminal"
#~ msgstr "於終端機執行"

#~ msgid "Show"
#~ msgstr "顯示"

#~ msgid "Execute"
#~ msgstr "執行"

#~ msgid "New folder"
#~ msgstr "新增資料夾"

#~ msgid "Delete"
#~ msgstr "刪除"

#~ msgid "Error while deleting files"
#~ msgstr "刪除檔案時發生錯誤"

#~ msgid "Are you sure you want to permanently delete these items?"
#~ msgstr "你確定要永久刪除這些項目嗎？"

#~ msgid "If you delete an item, it will be permanently lost."
#~ msgstr "如果直接刪除本項目，它會永久消失。"

#, fuzzy
#~ msgid "Show external disk drives in the desktop"
#~ msgstr "在桌面顯示個人資料夾"

#, fuzzy
#~ msgid "Show the external drives"
#~ msgstr "在桌面顯示個人資料夾"

#, fuzzy
#~ msgid "Show network volumes"
#~ msgstr "在《檔案》中顯示"

#~ msgid "Huge"
#~ msgstr "巨大圖示"
